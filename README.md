## React Black Jack

A simple black jack game written in React.
This project was written with the purpose of exploring React and to understand how to build and manipulate single page applications with it.

### Prerequisites

You would need to have npm installed to run the project.

### Installing

To get a copy of the project on your machine use the following commands:
```sh
git clone https://constructm@bitbucket.org/constructm/react-blackjack.git
```

```sh
cd react-blackjack
```

```sh
npm install
```

Once the project has finished installing run npm start and you're good to go.

### The Rules

The games has two players, that's you against the dealer.
When you click the New Game you will be presented with a view of cards that have bee dealt to you (Player) along with your score, as well as the dealers cards and the dealers score.
You will notice that the dealer's second card is always faced down when starting a new game. The face down card will be revealed once you hit the stand button which will be explained below.

Below the dealers cards are 4 buttons:

* Hit me: This button will add a card to your hand and increase your score based on the card you will have been given.
* Stand: The stand button is to indicate that you no longer or do not wish to add any cards to your hand. This will invoke the dealer to make their play and the face down card will be revealed.
* Reset: Starts the games from scracth. Resets scores and deals each player a new hand.
* Exit: Takes you back to the landing page of the application.

The aim of the game is to get a score of 21 or as close to 21 as possible without ever going over 21.
Your score is determined by your hand (cards issued to you). Each card's score, with the exception of the alphabet cards, is equivalent to that card's number. Cards Jack, King, Queen all have a value of 10. Ace is a special card as it's value is condtional. Ace can either be equal to 1 or 11 depending on your score. Ace will be 11 if your score is low and won't go over 21 with the addition of 11 and it's value will change to 1 if your score is high enough to go over 21 if you add 11.

The dealer will only play once you hit the stand button. The dealer will continue getting a new card as long as their score is less than 17. Once the dealer reaches a score of 17 or greater, the app will compare scores to determine the winner. The one with the highest score wins. Any player with a score greater than 21 automatically loses.