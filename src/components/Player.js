import React, { Component } from 'react';
import Hand from './Hand';

class Player extends Component{

    constructor(props) {
        super(props);

        // Initialize state
        this.state = {
            playerName: 'Player',
            score: 0,
            cards: []
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            cards: nextProps.player.cards,
            score: nextProps.player.score
        });
    }

    //adds a card to the cards state
    addCard(card){
        let cards = this.state.cards;
        cards.push(card);
        this.setState({cards: cards});
    }

    addCards(cards) {
        this.setState({cards: cards});
    }

    render(){
        return(<Hand playerName={this.state.playerName} score={this.state.score} cards={this.state.cards} />);
    }
}

export default Player;