import React, { Component } from 'react';
import uuid from 'uuid';
import DisplayCards from './DisplayCards';

class Hand extends Component {
    render(){

        let cards = [];
    
        if(this.props.cards) {
            
            cards = this.props.cards.map(card => {
                return (
                    <DisplayCards key={card.name+'-'+uuid.v4()} card={card} />
                );
            });
        };

        return(
            <div className="row top-content-50">
                <div className="row"><strong>{this.props.playerName}'s' score:</strong> {this.props.score}</div>
                <div className="row card-list">
                    {cards}
                </div>
            </div>
        );
    }
}

export default Hand;