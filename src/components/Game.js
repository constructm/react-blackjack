import React, { Component } from 'react';
import GameService from './GameService';

class Game extends Component {

  constructor(props) {
    super(props);

    this.state = {
      alertClass: 'alert-info',
      message: 'New game',
      actions: {
        alertMsg: this.alertMsg.bind(this)
      }
    };
  }

  // This method will be sent to the child component
  alertMsg(props) {
    this.setState({
      alertClass: props.class,
      message: props.message
    });
  }

  reset() {
    this.setState({
      alertClass: 'alert-info',
      message: 'New game'
    });
  }
  
  render() {

    return(
      <div>
        <div className='container text-center'>
            <h1>BlackJack</h1>
            <p className={'alert ' + this.state.alertClass}>{this.state.message}</p>

            <GameService actions={this.state.actions} />
        </div>

      </div>
    );

  }
}

export default Game;