import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Landing from './Landing';
import Game from './Game';

const Main = () => (
  <main>
    <Switch>
      <Route exact path='/' component={Landing}/>
      <Route path='/game' component={Game}/>
    </Switch>
  </main>
);

export default Main;