import React, { Component } from 'react';
import Hand from './Hand';

class Dealer extends Component{

    constructor(props) {
        super(props);

        this.state = {
            playerName: 'Dealer',
            score: 0,
            holeCard: {},
            cards: []
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            cards: nextProps.dealer.cards,
            score: nextProps.dealer.score,
            holeCard: nextProps.dealer.holeCard
        });
    }

    //assign a card to the hole card, the face down card
    initHole(card){
        this.setState({
            holeCard: card
        });
    }

    //adds a card to the cards state
    addCard(card){
        let cards = this.state.cards;
        cards.push(card);
        this.setState({cards: cards});
    }

    //reveal the hole card
    revealHoleCard() {
         let cards = this.state.cards;
         cards[1] = this.state.holeCard;
         this.setState({cards: cards});
    }

    render(){
        return(<Hand playerName={this.state.playerName} score={this.state.score} cards={this.state.cards} />);
    }
}

export default Dealer;