import React from 'react';
import { Link } from 'react-router-dom';
//import { Grid, Col, Button } from 'react-bootstrap';

const Landing = () => (
  <div className='container text-center'>
    <h1>BlackJack</h1>
    <p>Please click on 'New Game' to load a new game.</p>
    <Link className='btn btn-info btn-lg' to="/game">New Game</Link>
  </div>
);

export default Landing;