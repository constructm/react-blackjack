import React, { Component } from 'react';

class DisplayCards extends Component {
    render() {
        return (
            <li>
                <img src={ require('../assets/images/cards/'+this.props.card.image) } alt={this.props.card.name} title={this.props.card.name} width="60px" height="80px" />
            </li>
        );
    }
}

export default DisplayCards;