import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import CardService from './CardService';
import Player from './Player';
import Dealer from './Dealer';

class GameService extends Component {
  constructor(props) {
    super(props);
    
    //initiialize state
    this.state = this.initialState();

    //this.loadDeck = this.loadDeck.bind(this);
    this.newGame = this.newGame.bind(this);
    this.reset = this.reset.bind(this);
    //this.drawCard = this.drawCard.bind(this);
    this.getCard = this.getCard.bind(this);
    this.removeCard = this.removeCard.bind(this);
    this.getRandomInt = this.getRandomInt.bind(this);
    this.hit = this.hit.bind(this);
    this.stand = this.stand.bind(this);
    //this.revealHoleCard = this.revealHoleCard.bind(this);
    //this.calculateScore = this.calculateScore.bind(this);
    //this.addPlayerCard = this.addPlayerCard.bind(this);
  }

  componentWillMount() {
    this.loadDeck(); //Load deck after the component has mounted
  }

  componentDidMount() {
    this.newGame();
  }

  // Initial state. To be used in constructor and reset()
  initialState() {

    let deck = CardService.cards;
    let usedCards = [];

    // Merge card deck with used cards when resetting the game
    if(typeof this.state !== 'undefined') {
      if( typeof this.state.usedCards !== 'undefined' && this.state.usedCards.length > 0) {
        deck.concat(usedCards);
      }
    }

    return({
      cards: deck,
      usedCards: usedCards,
      player: {
        cards: [],
        score: 0
      },
      dealer: {
        cards: [],
        holeCard: {},
        score: 0
      }
    });
  }

  //initialize game
  newGame() {
    
    // Draw two cards for the player
    this.addPlayerCard(this.drawCard(2));
    
    // Set dealer's hand
    this.addDealerCard(this.drawCard(1));
    this.addDealerCard(this.getCard(0));

    // Enable or disable hit button
    this.state.player.score < 21 ? this.refs.hit.removeAttribute("disabled") : this.refs.hit.setAttribute("disabled", "disabled");
    
    // Enable stand button
    this.refs.stand.removeAttribute("disabled");
  }

  //reset
  reset() {

    //reset state
    this.setState(
      this.initialState(), 
    function(){
      this.newGame(); //start new game
      this.props.actions.alertMsg({
        class: 'alert-info',
        message: 'Think you can take a hit?.'
      });
    });
  }

  //adds a card to the cards state
  addPlayerCard(card){
    let cards = this.state.player.cards;
   
    if(Array.isArray(card)){
      for(let c of card) {
        cards.push(c);
      }
    }
    else {
      cards.push(card);
    }

    // Calcuate player score
    let playerScore = this.calculateScore(cards);
    
    // Updates player stats
    this.setState({player: {cards: cards, score: playerScore}}, function(){
      if(playerScore === 21) {
        this.props.actions.alertMsg({
          class: 'alert-success',
          message: 'Black Jack!'
        });
        this.refs.hit.setAttribute("disabled", "disabled");
      }
      if(playerScore > 21) {
        this.props.actions.alertMsg({
          class: 'alert-danger',
          message: 'BUST!!!'
        });
        this.refs.hit.setAttribute("disabled", "disabled");
      }
    });
  }

  //adds a card to the cards state
  addDealerCard(card){
    let cards = this.state.dealer.cards;
    let holeCard = this.state.dealer.holeCard;

    // set dealer's hole card
    if(Object.keys(holeCard).length === 0) holeCard = this.drawCard(1);

    if(Array.isArray(card)){
      for(let c of card) {
        cards.push(c);
      }
    }
    else {
      cards.push(card);
    }

    // Calcuate dealer score
    let dealerScore = this.calculateScore(cards);

    // Updates dealer stats
    this.setState({dealer: {cards: cards, score: dealerScore, holeCard: holeCard}});
  }

  // reveal hole card
  revealHoleCard() {
    let dCards = this.state.dealer.cards;
    dCards[1] = this.state.dealer.holeCard;
    let newScore = this.calculateScore(dCards);

    this.setState({dealer: {cards: dCards, score: newScore, holeCard: this.state.dealer.holeCard }}, function(){
      //console.log(this.state.dealer);
    });
  }

  //A fresh deck of cards
  loadDeck() {
    let cards = CardService.cards;
    this.setState({cards: cards});
  }

  hit() {
    //Only allow player to hit if their score is less than 21
    let pScore = this.state.player.score;
    if(pScore < 21) {
      // Draw card and add it to player's hand
      this.addPlayerCard(this.drawCard(1));

      this.props.actions.alertMsg({
        class: 'alert-info',
        message: 'Another hit perhaps?'
      });
    }
    else {
      this.refs.hit.setAttribute("disabled", "disabled");
      alert('Your current score is over 21. Please stand.');
    }
  }

  // When player is done making their play, dealer plays
  stand() {
    // Disable hit button
    this.refs.hit.setAttribute("disabled", "disabled");

    // Disable stand button
    this.refs.stand.setAttribute("disabled", "disabled");

    // Reveal hole card | Replace facedown card with hold card
    this.revealHoleCard();

    // While dealer score is less than 17
    let dScore = this.state.dealer.score;
    let dCards = this.state.dealer.cards;
    console.log(dScore);
    while(dScore < 17)
    {
      dCards.push(this.drawCard(1));
      dScore = this.calculateScore(dCards);
    }

    // save dealer cards to dealer stats
    this.setState({
      dealer: {
        cards: dCards,
        score: dScore,
        holeCard: this.state.dealer.holeCard
      }
    }, 
    function(){
        if(dScore > 21) {
          this.props.actions.alertMsg({
            class: 'alert-warning',
            message: 'Dealer Bust!'
          });
        }
        if(dScore === 21) {
          this.props.actions.alertMsg({
            class: 'alert-warning',
            message: 'Dealer Black Jack!'
          });
        }

        //compare final scores
        let pScore = this.state.player.score;
        if(dScore > 21 && pScore > 21) {
          this.props.actions.alertMsg({
            class: 'alert-info',
            message: 'You both bust. No winners for this round.'
          });
        }
        else {
          console.log('Checking scores...');

          if(pScore < 22 && dScore < 22) {

            if(pScore === dScore) {
              console.log('It is a tie');
              this.props.actions.alertMsg({
                class: 'alert-warning',
                message: 'It is a tie. Another round? Hit reset.'
              });
            }

            if(pScore > dScore) {
                console.log('Player wins');
                this.props.actions.alertMsg({
                  class: 'alert-success',
                  message: 'You WIN!.'
                });
            }

            if(dScore > pScore) {
                console.log('Dealer wins');
                this.props.actions.alertMsg({
                  class: 'alert-danger',
                  message: 'Sorry... Dealer wins.'
                });
            }
          }
          else {

            if(pScore < 22 && dScore > 21) {
              this.props.actions.alertMsg({
                class: 'alert-success',
                message: 'Dealer Bust! You WIN!'
              });
            }

            if(dScore < 22 && pScore > 21) {
              console.log('Dealer wins');
              this.props.actions.alertMsg({
                class: 'alert-danger',
                message: 'BUST!!! Dealer wins.'
              });
            }
          }
        }
      }
    );
  }

  // Make a play
  drawCard(howMany = 1)
  {
    console.log('Drawing card...');
    let cards = [];
    
    for (let step = 0; step < howMany; step++)
    {
      //Get a card
      var cardIndex = this.getRandomInt();
      var card = this.getCard(cardIndex);

      //Remove card from deck
      this.removeCard(cardIndex);

      cards.push(card);
    }

    return cards.length === 1 ? cards[0] : cards;
  }

  // Selects a specific card from the deck
  getCard(cardIndex)
  {
    console.log('Getting card...');
    return this.state.cards[cardIndex];
  }

  // Removes a played card from the deck
  removeCard(cardIndex)
  {
    // Prevent card back (card index 0) from being removed from the cards array
    if(cardIndex > 0)
    {
      // Added card to used cards
      let usedCards = this.state.cards;
      usedCards.push(this.state.cards[cardIndex]);
      this.setState({usedCards: usedCards});

      // Remove specific card from cards array
      this.state.cards.splice(cardIndex,1);
    }
  }

  //Calculate overall hand score | Calculate sum of player card values
  calculateScore(playerCards)
  {
    let score = 0;

    /**
     * TODO:
     * Determine if cards amount to overranking blackjack
     */

    //Determine player's score
    for(let card of playerCards)
    {
      let cardPoint = card.value;

      if(card.value === 11) {
          // Change value of ace to 1 if current player score plus current ace value amount to more than 21
          if(score + card.value > 21) cardPoint = 1;
      }

      score += cardPoint;
    }

    return score;
  }

  // Generates a random number
  getRandomInt() 
  {
    var min = 1;
    var max = this.state.cards.length - 1;
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  render() {

    return (
      <div>
        <Player player={this.state.player} />
        <Dealer dealer={this.state.dealer} />
        <div className="row top-content-20">
            <button className="btn btn-default btn-lg" ref="hit" onClick={this.hit}>Hit me</button>
            &nbsp;
            <button className="btn btn-default btn-lg" ref="stand" onClick={this.stand}>Stand</button>
            &nbsp;
            <button className="btn btn-warning btn-lg" onClick={this.reset}>Reset</button>
            &nbsp;
            <Link className='btn btn-danger btn-lg' to='/'>Exit</Link>
        </div>
      </div>
    );
  }

}

export default GameService;