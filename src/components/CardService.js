import cardData from '../assets/images/cards/_cards.json';

//let cardJson = require('../assets/images/cards/_cards.json');
// A simple data API that will be used to get the data for our
// components. On a real website, a more robust data fetching
// solution would be more appropriate.
const CardService = {
  cards: cardData,
  all: function() { return this.cards},
  get: function(name) {
    const isCard = c => c.name.toLowerCase() === name.replace(/ /g,'').toLowerCase();
    return this.cards.find(isCard) === undefined ? false : this.cards.find(isCard);
  },
  getByIndex: function(index) {
      return this.cards[index] === undefined ? false : this.cards[index];
  }
}

console.log(CardService);

export default CardService;